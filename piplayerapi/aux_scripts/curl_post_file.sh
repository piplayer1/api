#!/bin/bash

if [ -n "$1" ]; 
then
	echo ""
else
	echo -e "No filename supplied\nWill now exit";
	exit 1
fi 

data=`echo "@"$1`
url=`echo "http://0.0.0.0:5001/upload/"$1`

curl -X POST $url \
-H "Content-Type: text/xml" \
--data-binary $data

#http://0.0.0.0:5001/upload/TheBride01.mp4 \

