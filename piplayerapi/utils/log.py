import logging
import os
import json
from logging.handlers import RotatingFileHandler
from piplayerapi.config import config_envs
from piplayerapi.utils.api_functions import create_dirs


def log(log_path):
    logger = logging.getLogger('PiPlayer API Log')
    log_format = logging.Formatter(

        json.dumps({'date': '%(asctime)s',
                    'name': '%(name)s',
                    'level': '%(levelname)s',
                    'msg': '%(message)s',
                    })
    )

    log_handler = RotatingFileHandler(filename=log_path,
                                      maxBytes=512000,
                                      backupCount=2)  # 512Kb log file
    log_handler.setFormatter(log_format)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)
    return logger


# if __name__ == '__main__':
env = os.getenv('PIPLAYERAPI_ENV', 'dev')
conf = config_envs.get(env)
create_dirs(conf.logs_dir)
logger = log(conf.api_log)  # create logger
# logger.debug(msg='hello')
# _ = StructuredMessage   # optional, to improve readability
# logger.debug(msg={'foo': 'bar', 'num': 123})
