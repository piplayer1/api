import os
from werkzeug.utils import secure_filename

# OS/FILE SYSTEM

def create_dirs(_dir: str) -> None:
    try:
        os.mkdir(_dir)
        os.chmod(_dir, 0o777)  # a+rw
    except Exception:
        pass


def file_size(_dir: str, fname: str) -> str:
    fn = os.path.join(_dir, fname)
    info = os.stat(fn)
    return str(info.st_size)


def save_file(_dir: str, fn: str, mode: str, data) -> None:
    filename = os.path.join(_dir, fn)
    filename_tmp = os.path.join(_dir, '.' + secure_filename(fn))
    with open(filename_tmp, mode) as fn_wb:
        fn_wb.write(data)
        os.rename(filename_tmp, filename)
