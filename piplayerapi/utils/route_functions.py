from flask import (make_response,
                   jsonify)
from piplayerapi.utils.log import logger


# FLASK / API
def abort_response(error: str, code: int):
    resp = make_response(jsonify({'error': error}), code)
    logger.error(msg="{} {}".format(code, error))
    return resp
