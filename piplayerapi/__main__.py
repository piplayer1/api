from piplayerapi.app import api
from piplayerapi.config import config_envs
from os import getenv

# set environment in shell: export PIPLAYERAPI_ENV=prd
# default: 'dev'
if __name__ == '__main__':
    env = getenv('PIPLAYERAPI_ENV', 'dev')
    print('Enviroment: {}'.format(env))
    conf = config_envs.get(env)
    api.run(host=conf.host, port=conf.port, debug=conf.debug)
