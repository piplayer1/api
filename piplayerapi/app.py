#!/usr/bin/env python3
import os
import shlex
import subprocess
import sys
import requests
from pprint import pprint
from flask import (Flask,
                   jsonify,
                   make_response,
                   abort,
                   request)

from piplayerapi.config import config_envs
from piplayerapi.utils.api_functions import (create_dirs,
                                             file_size,
                                             save_file)
from piplayerapi.utils.route_functions import abort_response
from piplayerapi.utils.log import logger


env = os.getenv('PIPLAYERAPI_ENV', 'dev')
logger.info(msg='Application: {}'.format(__name__))
logger.info(msg='ENV: {}'.format(env))


api = Flask(__name__)
conf = config_envs.get(env)

try:
    requests.get(f'http://{conf.webui_host}:{conf.webui_port}/addhost')  # add this api host to piplayerwebui
except Exception:
    pass

@api.route('/')
def info():
    response = {'name': api.name,
                'info': {'python': sys.version,
                         'basedir': conf.basedir,
                         'dir_useruploads': conf.dir_useruploads,
                         'playerscript': conf.playerscript,
                         'app log': conf.api_log,
                         'player': conf.player,
                         'hostname': os.uname()[1],
                         }}
    return jsonify(response)


@api.route('/files')
def listfiles():
    create_dirs(conf.dir_useruploads)
    files = os.listdir(conf.dir_useruploads)
    files_dict = {fn: {'size(bytes)': file_size(conf.dir_useruploads, fn)}
                  for fn in files}
    return jsonify(files_dict)


@api.route('/upload/<fn>', methods=['POST'])
def upload(fn):

    if "/" in fn or not request.data:
        abort(abort_response(error='No filename or data given',
                             code=400))

    ext = ((fn.split('.'))[-1]).lower()
    if ext not in conf.allowed_formats:
        txt = 'File format is not: {}'.format(conf.allowed_formats)
        abort(abort_response(error=txt, code=400))

    create_dirs(conf.dir_useruploads)
    save_file(_dir=conf.dir_useruploads,
              fn=fn,
              mode='wb',
              data=request.data)
    # filename = os.path.join(conf.dir_useruploads, fn)
    # filename_tmp = os.path.join(conf.dir_useruploads,
    #                             '.' + secure_filename(fn))
    # with open(filename_tmp, 'wb') as fn_wb:
    #     fn_wb.write(request.data)
    #     os.rename(filename_tmp, filename)
    logger.debug(msg="Uploaded File: {}".format(fn))
    return make_response(jsonify({'file': fn, 'uploaded': True}, 200))


@api.route('/delete/<fn>', methods=['GET'])
def delete(fn):
    files_list = os.listdir(conf.dir_useruploads)
    if "/" in fn:
        abort(400)
    elif fn not in files_list:
        txt = '{} not present in uploads directory'.format(fn)
        abort(abort_response(error=txt, code=400))
    os.remove(os.path.join(conf.dir_useruploads, fn))
    logger.debug(msg="Removed File: {}".format(fn))
    return make_response(jsonify({'file': fn, 'deleted': True}), 200)


@api.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@api.errorhandler(400)
def error(error):
    return abort_response(error='Something wrong with your request', code=400)


if __name__ == '__main__':
    api.run(host='0.0.0.0', port=5001, debug=True)
