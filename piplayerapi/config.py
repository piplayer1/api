import os


class Config:
    debug = False
    # all common values to 3 environemnts
    basedir = '/home/andre/Documents/Projects/PiPlayer/'
    dir_useruploads = os.path.join(basedir, 'media/')
    dir_playerscript = os.path.join(basedir, 'playerscripts/')
    playerscript = os.path.join(dir_playerscript, 'playme.sh')
    logs_dir = os.path.join(basedir, 'logs/')
    api_log = os.path.join(logs_dir, 'api.log')
    allowed_formats = ['mp4', 'mov', 'avi', 'ogv', 'mp3', 'ogg', 'flac', 'wav']


class DevelopmentConfig(Config):
    debug = True
    host = '0.0.0.0'
    port = 5001
    player = 'mpv'
    webui_host = '0.0.0.0'
    webui_port = '5000'


class testingConfig(Config):
    debug = True
    testing = True
    host = '0.0.0.0'
    port = 5001


class ProductionConfig(Config):
    debug = False
    host = None  # '0.0.0.0'
    port = None  # 5001
    player = 'oxm'
    # TODO: setting for gunicorn


config_envs = dict(
    dev=DevelopmentConfig,
    test=testingConfig,
    prd=ProductionConfig
)