# PiPlayer-API
Runs on each of Pi of the Piplayer network.

It receives calls from the PiPlaye-UI to
* upload files to disk
* set & test the player

## Run
`cd piplayerapi`

`python -m piplayerapi dev`
